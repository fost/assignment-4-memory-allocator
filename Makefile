CFLAGS=--std=c17 -Wall -pedantic -Isrc/ -ggdb -Werror -Wextra -DDEBUG
BUILDDIR=build
SRCDIR=src
C=gcc

all: $(BUILDDIR)/mem.o $(BUILDDIR)/util.o $(BUILDDIR)/mem_debug.o $(BUILDDIR)/test.o $(BUILDDIR)/main.o
	$(C) -o $(BUILDDIR)/main $^

build:
	mkdir -p $(BUILDDIR)


$(BUILDDIR)/mem.o: $(SRCDIR)/mem.c $(BUILDDIR)
	$(C) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/mem_debug.o: $(SRCDIR)/mem_debug.c $(BUILDDIR)
	$(C) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/util.o: $(SRCDIR)/util.c $(BUILDDIR)
	$(C) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/test.o: $(SRCDIR)/test.c $(BUILDDIR)
	$(C) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/main.o: $(SRCDIR)/main.c $(BUILDDIR)
	$(C) -c $(CFLAGS) $< -o $@


clean:
	rm -rf $(BUILDDIR)
